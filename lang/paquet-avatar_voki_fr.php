<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'avatar_voki_description' => '{{Un Avatar VOKI dans vos articles et/ou vos squelettes.}}

Ce plugin ajoute la balise <code>#AVATAR_VOKI{options}</code> et le modèle "{{avatar_voki.html}}" pour vos squelettes SPIP, pour vous permettre d\'afficher votre Avatar VOKI (http://www.voki.com/) sur votre site Internet SPIP.',
	'avatar_voki_slogan'      => 'Un Avatar VOKI dans vos articles et/ou vos squelettes',
	'avatar_voki_nom'         => 'Avatar VOKI',
);
?>